package com.minelx.httpclient.core;

import com.minelx.httpclient.core.build.schema.RequestSchema;
import com.minelx.httpclient.core.request.HttpRequestFactory;

public class Requests {
	public static HttpRequestFactory parse(String source) {
		return new RequestSchema(source).render();
	}
}

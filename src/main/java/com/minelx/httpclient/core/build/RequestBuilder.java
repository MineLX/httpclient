package com.minelx.httpclient.core.build;

import com.minelx.httpclient.core.request.Get;
import com.minelx.httpclient.core.request.Post;
import com.minelx.httpclient.core.request.Put;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.util.Collections;
import java.util.Map;

public class RequestBuilder {
	private final String url;

	private Map<String, String> headers;

	private HttpEntity dataEntity;

	public RequestBuilder(String url) {
		this.url = url;
		headers = Collections.emptyMap();
		dataEntity = null;
	}

	public Get get() {
		return new Get(url, headers);
	}

	public Post post() {
		return new Post(url, headers, dataEntity);
	}

	public Put put() {
		return new Put(url, headers, dataEntity);
	}

	public RequestBuilder data(HttpEntity dataEntity) {
		this.dataEntity = dataEntity;
		return this;
	}

	public RequestBuilder headers(Map<String, String> headers) {
		this.headers = headers;
		return this;
	}

	public Map<String, String> headers() {
		return headers;
	}
}

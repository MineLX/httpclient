package com.minelx.httpclient.core.build.schema;

public class BadRequestSchemaException extends RuntimeException {
	public BadRequestSchemaException(String message) {
		super(message);
	}
}

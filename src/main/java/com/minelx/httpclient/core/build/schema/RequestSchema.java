package com.minelx.httpclient.core.build.schema;

import com.alibaba.fastjson.JSON;
import com.minelx.httpclient.core.build.RequestBuilder;
import com.minelx.httpclient.core.entity.*;
import com.minelx.httpclient.core.request.HttpRequestFactory;
import com.minelx.sequence.common.Templates;
import com.minelx.sequence.core.StringScanner;

import java.util.Map;

import static com.minelx.httpclient.core.entity.FormDataEntityFactory.toMap;
import static com.minelx.httpclient.core.entity.IEntityFactory.contentType;
import static com.minelx.sequence.core.StringScanner.source;

public class RequestSchema {

	private final String source;

	public RequestSchema(String source) {
		this.source = source;
	}

	public HttpRequestFactory render() {
		return render0(source);
	}

	public HttpRequestFactory render(Map<String, String> parameters) {
		return render0(new Templates(source).render(parameters));
	}

	// FIXME: 2021/7/30  NEEDED REFACTORING !!!
	private HttpRequestFactory render0(String source) {
		String[] lines = source.split("\r\n|\n");
		StringScanner methodAndUrlScanner = source(lines[0]); // method and url
		String method = methodAndUrlScanner.nextPage(); // POST OR GET
		methodAndUrlScanner.trim();
		String url = methodAndUrlScanner.til('\n');

		// deal with headers line
		Map<String, String> headers = toMap(JSON.parseObject(lines[2].substring(8)));

		// build
		RequestBuilder result = new RequestBuilder(url);

		// check if there is specified content-type
		if (headers.containsKey("Content-Type")) {
			IEntityFactory contentType = contentType(headers.get("Content-Type"));
			result.data(contentType.createEntity(lines[1].substring(6)));
		}
		result.headers(headers);

		// FIXME: 2021/7/30  NEEDED REFACTORING !!!
		switch (method) {
			case "GET":
				return result.get();
			case "POST":
				return result.post();
			case "PUT":
				return result.put();
			default:
				throw new UnsupportedOperationException("unsupported method: " + method);
		}
	}
}

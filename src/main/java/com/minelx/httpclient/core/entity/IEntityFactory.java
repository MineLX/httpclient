package com.minelx.httpclient.core.entity;

import com.minelx.httpclient.core.build.schema.BadRequestSchemaException;
import org.apache.http.HttpEntity;

public interface IEntityFactory {
	HttpEntity createEntity(String dataLine);

	static IEntityFactory contentType(String contentType) {
		if (contentType.contains("application/json")) {
			return new JSONEntityFactory();
		} else if (contentType.contains("application/x-www-form-urlencoded")) {
			return new FormDataEntityFactory();
		} else {
			throw new BadRequestSchemaException("Header 'Content-Type' is unknown: " + contentType);
		}
	}
}

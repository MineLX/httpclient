package com.minelx.httpclient.core.entity;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import java.nio.charset.StandardCharsets;

public class JSONEntityFactory implements IEntityFactory {
	@Override
	public HttpEntity createEntity(String dataLine) {
		StringEntity result = new StringEntity(dataLine, StandardCharsets.UTF_8);
		result.setContentEncoding("UTF-8");
		result.setContentType("application/json");
		return result;
	}
}

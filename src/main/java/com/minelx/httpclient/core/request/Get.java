package com.minelx.httpclient.core.request;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.Map;

public class Get implements HttpRequestFactory {

	private final String url;

	private final Map<String, String> headers;

	public Get(String url, Map<String, String> headers) {
		this.headers = headers;
		this.url = url;
	}

	@Override
	public HttpRequestBase createRequest() {
		HttpGet request = new HttpGet(url);
		for (Map.Entry<String, String> headerAsEntry : headers.entrySet()) {
			request.addHeader(headerAsEntry.getKey(), headerAsEntry.getValue());
		}
		return request;
	}
}

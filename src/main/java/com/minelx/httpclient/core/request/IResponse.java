package com.minelx.httpclient.core.request;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.function.Supplier;

public interface IResponse {
	// FIXME 2021/7/31  wait for me!!!     as collector of outputStream
	byte[] content();

	String contentAsText(Charset charset);

	void inject(OutputStream output);

	Map<String, String> headers();

	static IResponse of(byte[] bytes, Map<String, String> headers) {
		return new SimpleResponse(headers, bytes);
	}
}

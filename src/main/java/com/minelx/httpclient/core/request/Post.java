package com.minelx.httpclient.core.request;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.util.Map;

public class Post implements HttpRequestFactory {

	private final String url;

	private final Map<String, String> headers;

	private final HttpEntity dataEntity;

	public Post(String url, Map<String, String> headers, HttpEntity dataEntity) {
		this.url = url;
		this.headers = headers;
		this.dataEntity = dataEntity;
	}

	@Override
	public HttpRequestBase createRequest() {
		HttpPost result = new HttpPost(url);
		// pushing headers
		for (Map.Entry<String, String> headerAsEntry : headers.entrySet()) {
			result.addHeader(headerAsEntry.getKey(), headerAsEntry.getValue());
		}
		// building request entity
		result.setEntity(dataEntity);
		return result;
	}
}

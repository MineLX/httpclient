package com.minelx.httpclient.core.request;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.Map;

public class Put implements HttpRequestFactory {
	private final String url;

	private final Map<String, String> headers;

	private final HttpEntity dataEntity;

	public Put(String url, Map<String, String> headers, HttpEntity dataEntity) {
		this.url = url;
		this.headers = headers;
		this.dataEntity = dataEntity;
	}

	@Override
	public HttpRequestBase createRequest() {
		HttpPut result = new HttpPut(url);
		// adding headers
		for (Map.Entry<String, String> headerAsEntry : headers.entrySet()) {
			result.addHeader(headerAsEntry.getKey(), headerAsEntry.getValue());
		}
		// set data entity
		result.setEntity(dataEntity);
		// FIXME 2021/9/9  wait for me!!!     the same as how post request built
		return result;
	}
}

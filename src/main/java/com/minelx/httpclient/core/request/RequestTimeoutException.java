package com.minelx.httpclient.core.request;

public class RequestTimeoutException extends RuntimeException {
	public RequestTimeoutException(Exception causedBy) {
		super(causedBy);
	}
}

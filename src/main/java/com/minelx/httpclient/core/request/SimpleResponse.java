package com.minelx.httpclient.core.request;

import com.minelx.util.IOUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Map;

public class SimpleResponse implements IResponse {
	private final byte[] bytes;

	private final Map<String, String> headers;

	public SimpleResponse(Map<String, String> headers, byte[] bytes) {
		this.bytes = bytes;
		this.headers = headers;
	}

	@Override
	public String contentAsText(Charset charset) {
		return new String(bytes, charset);
	}

	@Override
	public byte[] content() {
		return bytes;
	}

	@Override
	public Map<String, String> headers() {
		return headers;
	}

	@Override
	public void inject(OutputStream output) {
		try {
			IOUtil.exchange(new ByteArrayInputStream(bytes), output);
		} catch (IOException e) {
			throw new RuntimeException("error while writing out bytes.", e);
		}
	}
}

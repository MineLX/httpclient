package com.minelx.httpclient.core.build;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.minelx.httpclient.core.entity.JSONEntityFactory;
import com.minelx.httpclient.core.request.Get;
import com.minelx.httpclient.core.request.IResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;

import static com.minelx.httpclient.core.request.PutTest.PUT_URL;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HttpRequestFactoryBuilderTest {

	public static final String GET_URL = "http://httpbin.org/get";

	public static final String POST_URL = "http://httpbin.org/post";

	public static final String HEADER_NAME = "Header-Name";

	public static final String HEADER_VALUE = "Header-Value";

	public static final String DATA_NAME = "Data-Name";

	public static final String DATA_VALUE = "Data-Value";

	@Test
	void put() {
		RequestBuilder builder = putURL()
				.data(new StringEntity(JSON.toJSONString(Collections.singletonMap(DATA_NAME, DATA_VALUE)), ContentType.APPLICATION_JSON))
				.headers(Collections.singletonMap(HEADER_NAME, HEADER_VALUE));
		IResponse response = builder.put().fetch(8000);
		JSONObject responseAsJSON = JSON.parseObject(response.contentAsText(UTF_8));
		assertEquals("{\"Data-Name\":\"Data-Value\"}", responseAsJSON.getString("data"));
		assertEquals(HEADER_VALUE, responseAsJSON.getJSONObject("headers").getString(HEADER_NAME));
	}

	@Test
	void get__no_header_added() {
		RequestBuilder builder = getRequestBuilder();
		Get get = builder.get();
		JSONObject response = JSON.parseObject(get.fetch(8000).contentAsText(UTF_8));
		assertEquals(GET_URL, response.get("url"));
	}

	@Test
	void get() {
		RequestBuilder requestBuilder = getRequestBuilder();
		requestBuilder.headers(new HashMap<>());
		requestBuilder.get();
		String responseAsText = requestBuilder.get().fetch(8000).contentAsText(UTF_8);
		JSONObject response = JSON.parseObject(responseAsText);
		assertEquals(GET_URL, response.getString("url"));
	}

	@Test
	void post__should_not_cause_exception_when_no_data_added() {
		// fetch with no data
		postRequestBuilder().post().fetch(8000).contentAsText(UTF_8);
	}

	@Test
	void post() {
		HashMap<String, String> headers = new HashMap<>();
		headers.put(HEADER_NAME, HEADER_VALUE);

		RequestBuilder requestBuilder = postRequestBuilder();
		requestBuilder.data(new JSONEntityFactory().createEntity(new JSONObject(Collections.singletonMap(DATA_NAME, DATA_VALUE)).toJSONString()));
		requestBuilder.headers(headers);
		requestBuilder.post();
		JSONObject responseAsJSON = JSON.parseObject(requestBuilder.post().fetch(8000).contentAsText(UTF_8));

		// verify response headers
		JSONObject responseHeaders = responseAsJSON.getJSONObject("headers");
		assertEquals(HEADER_VALUE, responseHeaders.getString(HEADER_NAME));

		// verify response data
		JSONObject dataAsJSON = JSON.parseObject(responseAsJSON.getString("data"));
		assertEquals(DATA_VALUE, dataAsJSON.getString(DATA_NAME));
	}

	static RequestBuilder putURL() {
		return new RequestBuilder(PUT_URL);
	}

	static RequestBuilder postRequestBuilder() {
		return new RequestBuilder(POST_URL);
	}

	static RequestBuilder getRequestBuilder() {
		return new RequestBuilder(GET_URL);
	}
}

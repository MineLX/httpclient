package com.minelx.httpclient.core.build.schema;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.minelx.httpclient.core.Requests;
import com.minelx.httpclient.core.request.Get;
import com.minelx.httpclient.core.request.HttpRequestFactory;
import com.minelx.httpclient.core.request.IResponse;
import com.minelx.landisk.filesystem.Paths;
import com.minelx.landisk.filesystem.core.Directory;
import com.minelx.landisk.filesystem.core.File;
import com.minelx.landisk.filesystem.core.init.OpenPolicy;
import org.apache.http.client.methods.HttpPut;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestSchemaTest {
	@Test
	void render__should_support_put_request() throws IOException {
		RequestSchema requestSchema = new RequestSchema(testFile("RequestSchemaTest__render__should_support_put_request.test").read(UTF_8));
		assertTrue(requestSchema.render().createRequest() instanceof HttpPut);
	}

	@Test
	void render__header_application_json() throws IOException {
		RequestSchema requestSchema = new RequestSchema(testFile("RequestSchemaTest__use_json_as_content_type.test").read(UTF_8));
		String text = requestSchema.render().fetch(8000).contentAsText(UTF_8);
		JSONObject responseAsJSON = JSON.parseObject(text);
		assertEquals("{\"data\":\"value\"}", responseAsJSON.getString("data"));
	}

	@Test
	void render() throws IOException {
		RequestSchema requestSchema = new RequestSchema(testFile("RequestSchemaTest__render.test").read(UTF_8));
		Map<String, String> parameters = new HashMap<>();
		String specifiedValue = "Header-Value";
		parameters.put("Header-Name", specifiedValue);
		HttpRequestFactory rendered = requestSchema.render(parameters);
		IResponse response = rendered.fetch(8000);
		JSONObject responseAsJSON = JSON.parseObject(response.contentAsText(UTF_8));
		JSONObject headers = responseAsJSON.getJSONObject("headers");
		assertEquals(specifiedValue, headers.getString("Header-Name"));
	}

	@Test
	void init() throws IOException {
		HttpRequestFactory httpRequestFactory = Requests.parse(testFile("/RequestSchemaTest__get_httpbin_schema.test").read(UTF_8));
		assertTrue(httpRequestFactory instanceof Get);

		// assert response
		IResponse response = httpRequestFactory.fetch(8000);
		JSONObject responseAsJSON = JSON.parseObject(response.contentAsText(UTF_8));
		JSONObject headers = responseAsJSON.getJSONObject("headers");
		assertEquals("Header-Value", headers.get("Header-Name"));
		assertEquals("application/x-www-form-urlencoded; charset=UTF-8", headers.get("Content-Type"));
	}

	public static File testFile(String fileName) {
		return testResources().child().file(fileName, OpenPolicy.useExisting());
	}

	public static Directory testResources() {
		return Paths.projectRoot()
				.child().dir("src", OpenPolicy.useExisting())
				.child().dir("test", OpenPolicy.useExisting())
				.child().dir("resources", OpenPolicy.useExisting());
	}
}

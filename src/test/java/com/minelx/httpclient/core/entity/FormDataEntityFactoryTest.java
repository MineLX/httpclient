package com.minelx.httpclient.core.entity;

import com.minelx.util.IOUtil;
import org.apache.http.HttpEntity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FormDataEntityFactoryTest {
	@Test
	void toEntity() throws IOException {
		assertEquals("1=2&3=4", entityAsText(new FormDataEntityFactory().createEntity("{\"1\":\"2\",\"3\":\"4\"}")));
	}

	private String entityAsText(HttpEntity entity) throws IOException {
		return IOUtil.stringOf(entity.getContent(), Charset.defaultCharset());
	}
}

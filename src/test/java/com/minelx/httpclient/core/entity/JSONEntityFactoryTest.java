package com.minelx.httpclient.core.entity;

import com.alibaba.fastjson.JSONObject;
import com.minelx.util.IOUtil;
import org.apache.http.HttpEntity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JSONEntityFactoryTest {
	@Test
	void simple_test() throws IOException {
		JSONObject data = new JSONObject();
		HttpEntity entity = new JSONEntityFactory().createEntity(data.toJSONString());
		assertEquals(data.toJSONString(), IOUtil.stringOf(entity.getContent(), Charset.defaultCharset()));
	}
}

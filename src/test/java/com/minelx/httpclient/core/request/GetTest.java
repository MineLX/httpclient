package com.minelx.httpclient.core.request;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.minelx.httpclient.core.build.HttpRequestFactoryBuilderTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static com.minelx.httpclient.core.request.PostTest.should_be_HTTP_BIN_response_headers;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetTest {

	private static final String HEADER_NAME = "Header-Name";

	private static final String HEADER_VALUE = "header-value";

	private Get get;

	@BeforeEach
	void setUp() {
		get = createGetRequest();
	}

	@Test
	void inject() {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		get.fetch(8000).inject(output);
		contentShouldRepresentTestedHeaders(output.toString());
	}

	@Test
	void fetch() {
		IResponse response = get.fetch(8000);
		contentShouldRepresentTestedHeaders(response.contentAsText(Charset.defaultCharset()));
		should_be_HTTP_BIN_response_headers(response.headers());
	}

	private void contentShouldRepresentTestedHeaders(String content) {
		JSONObject response = JSON.parseObject(content);
		assertEquals(HEADER_VALUE, response.getJSONObject("headers").get(HEADER_NAME));
	}

	private Get createGetRequest() {
		Map<String, String> headers = new HashMap<>();
		headers.put(HEADER_NAME, HEADER_VALUE);
		return new Get(HttpRequestFactoryBuilderTest.GET_URL, headers);
	}
}

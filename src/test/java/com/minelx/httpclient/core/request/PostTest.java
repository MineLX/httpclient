package com.minelx.httpclient.core.request;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.minelx.httpclient.core.build.HttpRequestFactoryBuilderTest;
import com.minelx.httpclient.core.entity.FormDataEntityFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PostTest {

	private static final String HEADER_NAME = "Header-Name";

	private static final String HEADER_VALUE = "header_value";

	private static final String DATA_NAME = "data_name";

	private static final String DATA_VALUE = "data_value";

	private Post post;

	@BeforeEach
	void setUp() {
		post = testedPost();
	}

	@Test
	void inject() {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		post.fetch(8000).inject(output);
		shouldRepresentTestedHeader(output.toString());
	}

	@Test
	void fetch() {
		IResponse response = post.fetch(8000);
		shouldRepresentTestedHeader(response.contentAsText(Charset.defaultCharset()));
		should_be_HTTP_BIN_response_headers(response.headers());
	}

	@Test
	void fetch__should_returns_the_content_of_response_entity() {
		shouldRepresentTestedHeader(post.fetch(8000).contentAsText(Charset.defaultCharset()));
	}

	private Post testedPost() {
		return new Post(HttpRequestFactoryBuilderTest.POST_URL,
				Collections.singletonMap(HEADER_NAME, HEADER_VALUE),
				new FormDataEntityFactory().createEntity(new JSONObject(Collections.singletonMap(DATA_NAME, DATA_VALUE)).toJSONString()));
	}

	private void shouldRepresentTestedHeader(String content) {
		JSONObject contentAsJSON = JSON.parseObject(content);
		assertEquals(HEADER_VALUE, contentAsJSON.getJSONObject("headers").get(HEADER_NAME));
	}

	public static void should_be_HTTP_BIN_response_headers(Map<String, String> headers) {
		assertEquals("application/json", headers.get("Content-Type"));
		assertEquals("gunicorn/19.9.0", headers.get("Server"));
	}
}

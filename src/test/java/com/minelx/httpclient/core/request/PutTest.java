package com.minelx.httpclient.core.request;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.minelx.httpclient.core.entity.FormDataEntityFactory;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PutTest {
	public static final String PUT_URL = "http://httpbin.org/put";

	private static final String HEADER_NAME = "Header-Name";

	private static final String HEADER_VALUE = "HEADER_VALUE";

	private static final String DATA_NAME = "DATA_NAME";

	private static final String DATA_VALUE = "DATA_VALUE";

	@Test
	void fetch() {
		Put request = new Put(PUT_URL,
				Collections.singletonMap(HEADER_NAME, HEADER_VALUE),
				new FormDataEntityFactory().createEntity(new JSONObject(Collections.singletonMap(DATA_NAME, DATA_VALUE)).toJSONString()));
		JSONObject responseAsJSON = JSON.parseObject(request.fetch(8000).contentAsText(UTF_8));
		assertEquals(DATA_NAME + "=" + DATA_VALUE, responseAsJSON.getString("data"));
		JSONObject headers = responseAsJSON.getJSONObject("headers");
		System.out.println("headers = " + headers);
		assertEquals(HEADER_VALUE, headers.getString(HEADER_NAME));
	}
}

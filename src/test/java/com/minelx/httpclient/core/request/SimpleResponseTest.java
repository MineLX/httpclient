package com.minelx.httpclient.core.request;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleResponseTest {
	@Test
	void inject() {
		IResponse response = IResponse.of("content".getBytes(), new HashMap<>());
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		response.inject(result);
	}

	@Test
	void init() {
		Map<String, String> headers = new HashMap<>();
		String content = "content";
		IResponse response = IResponse.of(content.getBytes(), headers);
		assertEquals(content, response.contentAsText(Charset.defaultCharset()));
		assertEquals(headers, response.headers());
	}
}
